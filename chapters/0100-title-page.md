\begin{titlepage}
    \begin{center}
        \vspace*{2.5cm}

        \huge
	State of the Edge: A Glossary\\

        \vspace{1.5cm}

        \Large
        The Authors \\

        \vspace{1.5cm}


        \vfill

        \normalsize
        Edited by:\\
        Ed Vielmetti\\

        \vspace{0.8cm}

        \normalsize
        Packet\\
	\monthname, \number \year
    \end{center}
\end{titlepage}
