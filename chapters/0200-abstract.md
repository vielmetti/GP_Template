# Abstract {.unnumbered}

This automation script provides a way to collaboratively work on a thesis. It will
allow students to write in their favorite way without caring too much if
at all about formatting. It is based on Markdown and \LaTeX. We use `pandoc`
software to convert from Markdown to \LaTeX, next we use `xelatex` to convert
from \LaTeX to PDF. We use Gitlab continuous integration to the compilation
online thus save the hassle of installing all the needed dependencies required
for the automation script to work.

\pagenumbering{roman}
